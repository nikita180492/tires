$(window).on('load', function () {
  $('.slider-banner').slick(
    {
      dots: true,
      arrows: true,
      adaptiveHeight: false, //подгоняет слайды под размер картинки
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1500,
      easing: 'ease',
      infinite: false,
      autoplay: true,
      autoplaySpeed: 1500,
      pauseOnFocus: true,
      pauseOnHover: true,
      pauseOnDotsHover: true,
      draggable: false, //на Пк запрешает мышкой перетягивать слайды
      swing: false, //на Моб запрешает перетягивать слайды
      touchThreshold: 5,
      touchMove: true,
      waitForAnimate: true,
      centerMode: false,
      variableWidth: false,
      rows: 1,
      slidesPreRow: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    }
  );

  $('.landing-catalog-slider').slick(
    {
      dots: false,
      arrows: false,
      adaptiveHeight: false, //подгоняет слайды под размер картинки
      slidesToShow: 5,
      slidesToScroll: 2,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 500,
      centerMode: false,
      variableWidth: true,
      waitForAnimate: true,
      easing: 'ease',
      responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },

        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    }
  );

  $('.custom-slider').slick(
    {
      dots: true,
      arrows: true,
      adaptiveHeight: false, //подгоняет слайды под размер картинки
      slidesToShow: 4,
      slidesToScroll: 1,
      infinite: true,
      centerMode: false,
      variableWidth: true,
      waitForAnimate: false,
      autoplay: true,
      autoplaySpeed: 500,
      easing: 'ease',
      touchMove: false,
      responsive: [
        {
          breakpoint: 1300,
          settings: {

            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true,
          }
        }
      ]
    }
  );
})



