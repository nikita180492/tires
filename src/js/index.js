// массив иконок svg

let x = ['.svg-icon']
x.forEach(item => {
  $(item).each(function () {
    let $img = $(this);
    let imgClass = $img.attr('class');
    let imgURL = $img.attr('src');
    $.get(imgURL, function (data) {
      let $svg = $(data).find('svg');
      if (typeof imgClass !== 'undefined') {
        $svg = $svg.attr('class', imgClass + ' replaced-svg');
      }
      $svg = $svg.removeAttr('xmlns:a');
      if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
        $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }
      $img.replaceWith($svg);
    }, 'xml');
  });
})

$(window).on('load', function () {
  // $('.preloader').fadeOut();

  $(".order-сall").click((e) => {
    e.stopPropagation()
    $(".form-call-area").addClass("_open")
  })

  $(".form-call-close").click(() => {
    $(".form-call-area").removeClass("_open")
  })

  //клик по булавке на карте, открытие tooltip
  $(".map-pin").click(function () {
    $(this).toggleClass("_active")
  })

//переключатель в модалке шиномлнтаж
  $(".switch-block").click(() => {
    if (!$(".switch-control input").is(":checked")) {
      $(".switch-block .switch-text:first-child").addClass('switch-text-select')
      $(".switch-block .switch-text:last-child").removeClass('switch-text-select')
    } else {
      $(".switch-block .switch-text:first-child").removeClass('switch-text-select')
      $(".switch-block .switch-text:last-child").addClass('switch-text-select')
    }
  })




  //открытие-закрытие левого меню
  $(".menu-content-close").click(() => {
    $(".left-mobile-menu-area").removeClass("_open")
  })

  $(".header-navigation-mobile").click(() => {
    $(".left-mobile-menu-area").addClass("_open")
  })

  $(".select-all").click(() => {
    let inputs = $(".cart-grid-col").find('.normal-checkbox')
    let btnSelect = $('.select-all')

    if (!btnSelect.attr('select')) {
      btnSelect.html('Снять выделение');
      btnSelect.attr('select', 'all');
      inputs.each((index, input) => {
        $(input).prop('checked', true);
      })

    } else {
      btnSelect.html('Выбрать все');
      btnSelect.removeAttr('select');
      inputs.each((index, input) => {
        $(input).prop('checked', false);
      })
    }

  })

  $(document).on('click', '.fancybox-bg', function () {
    $.fn.fancybox.close();
  });

  $(".change-filter-chosen").click(function () {

    // $(this).parents(".catalog-filter-chosen-area").find(".filter-chosen-container").toggleClass("_open")
    $(this).parents(".catalog-filter-chosen-area").find(".filter-chosen-container").slideToggle("slow", () => {

    })

  })


  $(".select").click(function () {
    $(this).toggleClass("_open")
  })

  $(".product-match").click(function () {
    $(this).parents(".card-product").find('.match-drop-area').toggleClass('_open')
  })
  $(".product-match-row").click(function () {
    $(this).parents(".card-product").find(".match-slide-toggle-area").slideToggle("slow", () => {

    })
  })

  $(".view-row").click(function () {
    $(this).addClass("_active")
    $('.view-col').removeClass("_active")
    $('.container-product .row').addClass('view-row-style')

  })
  $(".view-col").click(function () {
    $(this).addClass("_active")
    $('.view-row').removeClass("_active")
    $('.container-product .row').removeClass('view-row-style')
  })

  $(".search-input").click(function (e) {
    e.preventDefault()
    e.stopPropagation()
    $(this).addClass("_open")
  })

  //открываем выпадаюший список товаров в корзине, при клике на корзину в шапке сайта
  $(".cart-area").click((e) => {
    // e.preventDefault()
    // e.stopPropagation()
    $(".drop-down-menu-cart").addClass("_open")
    e.stopPropagation()
  })

  $(window).click(function (e) {
    // e.stopPropagation()
    // e.preventDefault()
    $(".search-input").removeClass("_open")
    $(".drop-down-menu-cart").removeClass("_open")
  });



  let filter = $('.filter')
  let filterNextStep = $('.filter-step-next')

  $('.js-button-filter').click(function () {
    filter.addClass('filter--active')
    $('html').css('overflow-y', 'hidden')
  })

  $('.js-filter-block__button-all').click(function () {
    filterNextStep.toggleClass('filter-step-next--active')
  })

  $('.js-filter-header-back').click(function () {
    filterNextStep.removeClass('filter-step-next--active')
  })

  $('.js-filter-close').click(function () {
    filter.removeClass('filter--active')
    filterNextStep.removeClass('filter-step-next--active')
    $('html').css('overflow-y', 'auto')
  })

  $('.js-catalog-select').click(function () {
    $('.catalog-select__options').toggleClass('catalog-select__options--active')
  })
})
